import sys
import zlib
import json
import logging
import hashlib
import threading
from socket import socket
from datetime import datetime

from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes
from PyQt5.QtWidgets import (QApplication, QMainWindow, QDesktopWidget, QTextEdit,
                             QPushButton, QVBoxLayout, QHBoxLayout, QLineEdit, QWidget)

from protocol import make_request
from utils import get_chunk


class Application:
    def __init__(self, host, port, buffersize):
        self._host = host
        self._port = port
        self._buffersize = buffersize
        self._sock = socket()

    def __enter__(self):
        self._sock.connect((self._host, self._port))
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        message = 'Client shutdown'
        if exc_type:
            if not exc_type is KeyboardInterrupt:
                message = 'Client stopped with error'
        logging.info(message, exc_info=exc_val)
        return True

    def connect(self):
        self._sock.connect((self._host, self._port))

    def read(self):
        while True:
            comporessed_response = self._sock.recv(self._buffersize)
            encrypted_response = zlib.decompress(comporessed_response)

            nonce, encrypted_response = get_chunk(encrypted_response, 16)
            key, encrypted_response = get_chunk(encrypted_response, 16)
            tag, encrypted_response = get_chunk(encrypted_response, 16)

            cipher = AES.new(key, AES.MODE_EAX, nonce)

            raw_responce = cipher.decrypt_and_verify(encrypted_response, tag)
            logging.info(raw_responce.decode())
            response = json.loads(raw_responce)
            data = response.get('data')
            self.display_text.append(data.get('data'))

    def render(self):
        app = QApplication(sys.argv)
        window = QMainWindow()
        window.setGeometry(400, 600, 400, 600)

        central_widget = QWidget()

        self.display_text = QTextEdit()
        self.display_text.setReadOnly(True)
        self.enter_text = QTextEdit()
        self.send_button = QPushButton('Send', window)
        self.enter_text.setMaximumHeight(64)
        self.send_button.setMaximumHeight(64)

        base_layout = QVBoxLayout()
        top_layout = QHBoxLayout()
        footer_layout = QHBoxLayout()

        top_layout.addWidget(self.display_text)
        footer_layout.addWidget(self.enter_text)
        footer_layout.addWidget(self.send_button)

        base_layout.addLayout(top_layout)
        base_layout.addLayout(footer_layout)

        central_widget.setLayout(base_layout)
        window.setCentralWidget(central_widget)

        dsk_widget = QDesktopWidget()
        geometry = dsk_widget.availableGeometry()
        center_position = geometry.center()
        frame_geometry = window.frameGeometry()
        frame_geometry.moveCenter(center_position)
        window.move(frame_geometry.topLeft())

        self.send_button.clicked.connect(self.write)

        window.show()
        sys.exit(app.exec_())

    def write(self):
        key = get_random_bytes(16)
        cipher = AES.new(key, AES.MODE_EAX)

        hash_obj = hashlib.sha256()
        hash_obj.update(
            str(datetime.now().timestamp()).encode()
        )

        data = self.enter_text.toPlainText()

        request = make_request('echo', {'data': data}, hash_obj.hexdigest())
        bytes_request = json.dumps(request).encode()
        encrypted_request, tag = cipher.encrypt_and_digest(bytes_request)

        bytes_request = zlib.compress(
            b'%(nonce)s%(key)s%(tag)s%(data)s' % {
                b'nonce':cipher.nonce, b'key':key, b'tag': tag, b'data': encrypted_request
            }
        )
        self.enter_text.clear()
        self._sock.send(bytes_request)

    def run(self):
        read_thread = threading.Thread(target=self.read)
        read_thread.start()

        self.render()
