.. server documentation master file, created by
   sphinx-quickstart on Tue Sep  3 01:19:44 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to server's documentation!
==================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:


Modules

- auth
- echo
- messenger


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
